package com.example.gameadvisor;

import com.example.gameadvisor.editeur.EditeurService;
import com.example.gameadvisor.jeu.JeuService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EditeurTest {


    @Autowired
    EditeurService editeurService;

    @Test
    public void NotVoidEditeurList() {
        int taille = editeurService.findAll().size();
        assertNotEquals(taille,0);
    }

}
