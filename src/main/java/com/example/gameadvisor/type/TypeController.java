package com.example.gameadvisor.type;

import com.example.gameadvisor.MessageController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
public class TypeController extends MessageController {

    @ModelAttribute("module")
    String module() {
        return "types";
    }

    private final TypeService typeService;

    @Autowired
    public TypeController(TypeService typeService) {
        this.typeService = typeService;
    }

    @GetMapping("/types")
    public String main(Model model) {
        model.addAttribute("items", this.typeService.findAll());
        addMessageToModel(model);
        return "items";
    }

    @PostMapping("/types")
    public String add(@RequestParam(required = false) String nom) {
        if (!nom.isEmpty()) {
            typeService.add(new Type(nom));
            setSuccessMessage("Le type \"" + nom + "\" à bien été ajouté.");
        }
        return "redirect:/types";
    }

    @PostMapping("/types/{id}")
    public String update(@PathVariable Long id, @RequestParam() String nom) {
        Optional<Type> type = typeService.getById(id);
        if (type.isPresent()) {
            typeService.update(type.get(), nom);
            setSuccessMessage("Le type à bien été renommé en \"" + nom + "\".");
        } else {
            setErrorMessage("Le type n'a pas pu être modifié.");
        }
        return "redirect:/types";
    }

    @GetMapping("/types/{id}/supprimer")
    public String delete(@PathVariable Long id) {
        if (typeService.getById(id).isPresent()) {
            typeService.deleteById(id);
        }
        setSuccessMessage("Le type à bien été supprimé.");
        return "redirect:/types";
    }
}
