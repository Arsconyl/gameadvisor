package com.example.gameadvisor.editeur;

import com.example.gameadvisor.MessageController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
public class EditeurController extends MessageController {

    @ModelAttribute("module")
    String module() {
        return "editeurs";
    }

    private final EditeurService editeurService;

    @Autowired
    public EditeurController(EditeurService editeurService) {
        this.editeurService = editeurService;
    }

    @GetMapping("/editeurs")
    public String main(Model model) {
        model.addAttribute("items", this.editeurService.findAll());
        addMessageToModel(model);
        return "items";
    }

    @PostMapping("/editeurs")
    public String add(@RequestParam(required = false) String nom) {
        if (!nom.isEmpty()) {
            editeurService.add(new Editeur(nom));
            setSuccessMessage("L'éditeur \"" + nom + "\" à bien été ajouté.");
        }
        return "redirect:/editeurs";
    }

    @PostMapping("/editeurs/{id}")
    public String update(@PathVariable Long id, @RequestParam() String nom) {
        Optional<Editeur> editeur = editeurService.getById(id);
        if (editeur.isPresent()) {
            editeurService.update(editeur.get(), nom);
            setSuccessMessage("L'éditeur à bien été renommé en \"" + nom + "\".");
        } else {
            setErrorMessage("L'éditeur n'a pas pu être modifié.");
        }
        return "redirect:/editeurs";
    }

    @GetMapping("/editeurs/{id}/supprimer")
    public String delete(@PathVariable Long id) {
        editeurService.deleteById(id);
        setSuccessMessage("L'éditeur et tous ses jeux ont bien été supprimés.");
        return "redirect:/editeurs";
    }
}
