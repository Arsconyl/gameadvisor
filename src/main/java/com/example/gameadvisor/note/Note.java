package com.example.gameadvisor.note;

import com.example.gameadvisor.jeu.Jeu;
import org.springframework.web.context.annotation.ApplicationScope;

import javax.persistence.*;
import java.util.Objects;

@ApplicationScope
@Entity
@Table(name="note_jeu")
public class Note {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "note")
    private int note;

    @Column(name="nom_testeur")
    private String testeur;

    @ManyToOne(targetEntity = Jeu.class)
    @JoinColumn(name = "id_jeu", referencedColumnName = "id", nullable = false)
    private Jeu jeu;

    public Note(){}

    public Note(Long id, int note, String testeur, Jeu jeu) {
        this.id = id;
        this.note = note;
        this.testeur = testeur;
        this.jeu = jeu;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public String getTesteur() {
        return testeur;
    }

    public void setTesteur(String testeur) {
        this.testeur = testeur;
    }

    public Jeu getJeu() {
        return jeu;
    }

    public void setJeu(Jeu jeu) {
        this.jeu = jeu;
    }

    @Override
    public String toString() {
        return "Note{" +
                "id=" + id +
                ", note=" + note +
                ", testeur='" + testeur + '\'' +
                ", jeu=" + jeu +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Note note = (Note) o;
        return Objects.equals(id, note.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
