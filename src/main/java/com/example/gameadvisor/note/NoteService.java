package com.example.gameadvisor.note;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class NoteService {
    private final NoteRepository noteRepository;

    @Autowired
    public NoteService(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    public Collection<Note> findAll()
    {
        return noteRepository.findAll();
    }

    public Optional<Note> getById(Long id) {
        return noteRepository.findById(id);
    }

    public Note add(Note note)
    {
        return noteRepository.save(note);
    }

    public Note insert(Long id, Note note)
    {
        note.setId(id);
        return noteRepository.save(note);
    }

    public void deleteById(Long id)
    {
        noteRepository.deleteById(id);
    }
}
