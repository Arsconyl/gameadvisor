package com.example.gameadvisor.theme;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class ThemeService {
    private final ThemeRepository themeRepository;

    @Autowired
    public ThemeService(ThemeRepository themeRepository) {
        this.themeRepository = themeRepository;
    }

    public Collection<Theme> findAll() {
        return themeRepository.findAll();
    }

    public Optional<Theme> getById(Long id) {
        return themeRepository.findById(id);
    }

    public Theme update(Theme theme, String nom)
    {
        theme.setNom(nom);
        return themeRepository.save(theme);
    }

    public Theme add(Theme theme)
    {
        return themeRepository.save(theme);
    }

    public Theme insert(Long id, Theme theme)
    {
        theme.setId(id);
        return themeRepository.save(theme);
    }

    public void deleteById(Long id)
    {
        themeRepository.deleteById(id);
    } 
}
