package com.example.gameadvisor.jeu;

import com.example.gameadvisor.MessageController;
import com.example.gameadvisor.editeur.Editeur;
import com.example.gameadvisor.editeur.EditeurService;
import com.example.gameadvisor.genre.Genre;
import com.example.gameadvisor.genre.GenreService;
import com.example.gameadvisor.theme.Theme;
import com.example.gameadvisor.theme.ThemeService;
import com.example.gameadvisor.type.Type;
import com.example.gameadvisor.type.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
public class JeuController extends MessageController {
    @ModelAttribute("module")
    String module() {
        return "accueil";
    }

    private final EditeurService editeurService;
    private final GenreService genreService;
    private final JeuService jeuService;
    private final ThemeService themeService;
    private final TypeService typeService;

    @Autowired
    public JeuController(TypeService typeService, EditeurService editeurService, GenreService genreService, JeuService jeuService, ThemeService themeService) {
        this.jeuService = jeuService;
        this.typeService = typeService;
        this.editeurService = editeurService;
        this.genreService = genreService;
        this.themeService = themeService;
    }

    @GetMapping("/")
    public String main(Model model) {
        addDataToModel(model);
        addMessageToModel(model);
        return "accueil";
    }

    @PostMapping("/jeux")
    public String searchGames(
            Model model,
            @RequestParam(required = false) Type type,
            @RequestParam(required = false) Genre genre,
            @RequestParam(required = false) Theme theme,
            @RequestParam(required = false) Editeur editeur,
            @RequestParam(required = false, defaultValue = "-1") int joueurs,
            @RequestParam(required = false, defaultValue = "-1") int age
    ) {
        model.addAttribute("jeux", this.jeuService.findJeuxByMultipleFilters(type, genre, theme, editeur, joueurs, age));
        model.addAttribute("jeuService", this.jeuService);
        addMessageToModel(model);
        return "jeux";
    }

    @GetMapping("/jeux/ajouter")
    public String ajouter(Model model) {
        addDataToModel(model);
        model.addAttribute("mode", "ajouter");
        addMessageToModel(model);
        return "formulaire-jeu";
    }

    @PostMapping("/jeux/ajouter")
    public String ajouter(
            Model model,
            @RequestParam(required = false) String nom,
            @RequestParam(required = false) Type type,
            @RequestParam(required = false) Genre genre,
            @RequestParam(required = false) Theme theme,
            @RequestParam(required = false) Editeur editeur,
            @RequestParam(name = "joueurs-min", defaultValue = "-1") int joueursMin,
            @RequestParam(name = "joueurs-max", defaultValue = "-1") int joueursMax,
            @RequestParam(defaultValue = "-1") int age
    ) {
        if (nom.isEmpty() || editeur == null || joueursMin <= 0 || joueursMax <= 0 || age <= 0 || joueursMax <= joueursMin) {
            setErrorMessage("Impossible de créer le jeu car le formulaire n'a pas été rempli correctement.");
            return "redirect:/jeux/ajouter";
        }

        jeuService.add(new Jeu(nom, type, genre, theme, editeur, age, joueursMin, joueursMax));
        setSuccessMessage("Le jeu \"" + nom + "\" a bien été ajouté.");

        return "redirect:/";
    }

    @GetMapping("/jeux/{id}/supprimer")
    public String delete(@PathVariable Long id) {
        if (jeuService.getById(id).isPresent()) {
            jeuService.deleteById(id);
        }
        setSuccessMessage("Le jeu à bien été supprimé.");
        return "redirect:/";
    }

    @GetMapping("/jeux/{id}/modifier")
    public String update(@PathVariable Long id, Model model) {
        Optional<Jeu> jeu = jeuService.getById(id);
        if (!jeu.isPresent()) {
            setErrorMessage("L'éditeur n'a pas pu être trouvé.");
            return "redirect:/";
        }

        model.addAttribute("jeu", jeu.get());
        addDataToModel(model);
        model.addAttribute("mode", "modifier");
        return "formulaire-jeu";
    }

    @PostMapping("/jeux/{id}/modifier")
    public String update(
            @PathVariable Long id,
            @RequestParam(required = false) String nom,
            @RequestParam(required = false) Type type,
            @RequestParam(required = false) Genre genre,
            @RequestParam(required = false) Theme theme,
            @RequestParam(required = false) Editeur editeur,
            @RequestParam(name = "joueurs-min", defaultValue = "-1") int joueursMin,
            @RequestParam(name = "joueurs-max", defaultValue = "-1") int joueursMax,
            @RequestParam(defaultValue = "-1") int age
    ) {
        if (nom.isEmpty() || editeur == null || joueursMin <= 0 || joueursMax <= 0 || age <= 0  || joueursMax <= joueursMin) {
            setErrorMessage("Impossible de modifier le jeu car le formulaire n'a pas été rempli correctement.");
            return "redirect:/jeux/" + id + "/modifier";
        }

        Optional<Jeu> jeu = jeuService.getById(id);
        if (jeu.isPresent()) {
            jeuService.update(jeu.get(), nom, type, genre, theme, editeur, age, joueursMin, joueursMax);
            setSuccessMessage("Le jeu \"" + nom + "\" a bien été modifié.");
        } else {
            setErrorMessage("Le jeu n'a pas pu être modifié.");
        }

        return "redirect:/";
    }

    private void addDataToModel(Model model) {
        model.addAttribute("types", this.typeService.findAll());
        model.addAttribute("jeux", this.jeuService.findAll());
        model.addAttribute("genres", this.genreService.findAll());
        model.addAttribute("themes", this.themeService.findAll());
        model.addAttribute("editeurs", this.editeurService.findAll());
    }
}
