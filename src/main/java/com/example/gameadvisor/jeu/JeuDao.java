package com.example.gameadvisor.jeu;

import com.example.gameadvisor.editeur.Editeur;
import com.example.gameadvisor.genre.Genre;
import com.example.gameadvisor.theme.Theme;
import com.example.gameadvisor.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class JeuDao {
    EntityManager em;

    @Autowired
    public JeuDao(EntityManager em) {
        this.em = em;
    }


    public List<Jeu> findJeuxByMultipleFilters(Type type, Genre genre, Theme theme, Editeur editeur, Integer nbJoueurs, Integer ageMini) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Jeu> cq = cb.createQuery(Jeu.class);

        Root<Jeu> jeu = cq.from(Jeu.class);
        List<Predicate> predicates = new ArrayList<>();
        if(type != null)
            predicates.add(cb.equal(jeu.get("type"), type.getId()));
        if(genre != null)
            predicates.add(cb.equal(jeu.get("genre"), genre.getId()));
        if(theme != null)
            predicates.add(cb.equal(jeu.get("theme"), theme.getId()));
        if(editeur != null)
            predicates.add(cb.equal(jeu.get("editeur"), editeur.getId()));
        if(nbJoueurs > 0)
           predicates.add(cb.and(cb.lessThanOrEqualTo(jeu.get("nbJoueurMini"), nbJoueurs),
                   cb.greaterThanOrEqualTo(jeu.get("nbJoueurMaxi"), nbJoueurs)));
        if(ageMini > 0)
            predicates.add(cb.lessThanOrEqualTo(jeu.get("ageMinimum"), ageMini));
        cq.where(predicates.toArray(new Predicate[0]));

        TypedQuery<Jeu> query = em.createQuery(cq);

        return query.getResultList();
    }
}
